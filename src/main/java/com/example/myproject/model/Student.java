package com.example.myproject.model;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Student {
	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private int age;
	public Student(){
	}
	public Student(String name){
		this.name=name;
	}
	public Student(String name,int age){
		this.name=name;
		this.age=age;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	public boolean equals(Object obj){
		if(obj instanceof Student){
			if(this.name.equals(((Student) obj).getName())&&this.age==((Student) obj).getAge())
				return true;
		}
		return false;
	}
	public int hashCode(){
		return (this.name+"_"+this.age).hashCode();
	}
}
