package com.example.myproject;


import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.parsing.ParseState;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import com.example.myproject.dao.StudentDao;
import com.example.myproject.model.Student;
@Controller
@EnableAutoConfiguration
@EntityScan
@EnableJpaRepositories
@ComponentScan
public class SampleController {
	@Value("${my.spring.data}")
	String myConfig;
	
	@Autowired
	StudentDao studentDao;
	
    @RequestMapping("/")
    @ResponseBody
    String home() {
    	Student u=new Student("Tom");
//    	studentDao.save(u);
        List<Student> rs= Arrays.asList(new Student("Tom",6),new Student("Tom",6),new Student("Tom",3),new Student("Tom",3),new Student("Sala",5));
        /*Map<String,Optional<Student>> mm= rs.stream().collect(
                Collectors.groupingBy(Student::getName,
                        Collectors.reducing((Student a,Student b)->{
                                    a.setAge(a.getAge()+b.getAge());
                                    return a;
                                }
                        )
                )
        );
        Map<String,Student> nn= rs.stream().collect(
                Collectors.groupingBy(Student::getName,
                        Collectors.reducing(new Student("",0),(a,b)->{
                                    a.setAge(a.getAge()+b.getAge());
                                    return a;
                                }
                        )
                )
        );*/
        System.out.println(rs);
        for (int i = 0; i <rs.size() ; i++) {
            System.out.println(rs.get(i).getName()+"_"+rs.get(i).getAge());
        }
        Map<String,Set<Student>> ll= rs.stream().collect(
                Collectors.groupingBy(Student::getName,
                        Collectors.toSet()
                )
        );
        /*for(Map.Entry<String,Optional<Student>> item : mm.entrySet()){
            //System.out.println(item.getKey()+":"+item.getValue().get().getAge());
        }
        for(Map.Entry<String,Student> item : nn.entrySet()){
            //System.out.println(item.getKey()+":"+item.getValue().getAge());
        }*/
        for(Map.Entry<String,Set<Student>> item : ll.entrySet()){
            System.out.println(item.getKey()+":"+item.getValue().size());
            for(Student x:item.getValue()){
                System.out.println(x.getName()+x.getAge());
            }
        }
        System.out.println("abd"+new Date()+true);
        return myConfig;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SampleController.class, args);
    }
}